--Q1
select * from bond where CUSIP = '28717RH95'
--Q2
select * from bond order by maturity
--Q3
select sum(quantity*price) from bond
--Q4
select CUSIP, quantity*(coupon/100) as 'product' from bond 
--Q5
select * from bond b
inner join rating r
on b.rating = r.rating 
where ordinal <= 3
--Q6
select rating, AVG(price) as 'Average Price', AVG(coupon/100) as 'Average Coupon'
from bond 
group by rating
--Q7
select bond.CUSIP, bond.coupon, bond.price 
from bond
inner join rating
on rating.rating = bond.rating
where (bond.coupon/bond.price) < (rating.expected_yield)
