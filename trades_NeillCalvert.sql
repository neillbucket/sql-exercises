--Q1
select * from trader 
inner join position 
on trader.ID = position.trader_ID
inner join trade topen
on position.opening_trade_ID = topen.ID
inner join trade tclose
on position.closing_trade_ID = tclose.ID
where trader.ID = 1

--Q2
select sum(t.size * t.price * (case when t.buy = 1 then -1 else 1 end))
from trade t inner join position p 
on p.opening_trade_id = t.id or p.closing_trade_id = t.id
where p.closing_trade_id != p.opening_trade_id
and p.closing_trade_id is not null;

--Q3
create view profit4all as
select

